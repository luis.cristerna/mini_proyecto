import 'package:dio/dio.dart';
import 'package:mini_proyecto/services/url-service.dart';

class DataService{

  Future<Map<dynamic,dynamic>> getData(String word) async {
    Map<dynamic,dynamic> result = {
      'error' : true,
    };
    try {
      Map<String, dynamic> queryParams = {
        'action' : 'query',
        'format' : 'json',
        'list' : 'search',
        'srsearch' : word,
        'utf8' : '1',
        'srlimit' : 50,
        'origin' : '*',
      };
      var response = await Dio().get(urlService.urlWikipedia, queryParameters: queryParams);
      result = {
        'error' : false,
        'data' : response.data
      };
    } catch (e) {
      print(e);
    }
    return result;
  }

}

DataService dataService = DataService();