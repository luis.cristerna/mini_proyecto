import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class ResultInfo extends StatelessWidget {
  final String appBarTittle;
  final String htmlData;
  final int pageid;
  final int wordcount;

  ResultInfo(
      {Key? key,
      required this.htmlData,
      required this.appBarTittle,
      required this.pageid,
      required this.wordcount})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          appBarTittle,
          style: TextStyle(color: Colors.black87),
        ),
        centerTitle: false,
        iconTheme: IconThemeData(
          color: Colors.black87, //change your color here
        ),
        automaticallyImplyLeading: true,
        backgroundColor: Colors.white,
        elevation: 1,
      ),
      body: ListView(
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 90,
            child: Image.asset(
              'assets/img/banner.jpeg',
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(8, 40, 8, 8),
            child: Text(
              appBarTittle,
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0.0),
            child: Html(data: htmlData),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'ID Pagina: ',
                    style: TextStyle(
                        color: Colors.black87, fontWeight: FontWeight.bold),
                  ),
                  TextSpan(
                    text: pageid.toString(),
                    style: TextStyle(color: Colors.black87),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Número de palabras: ',
                    style: TextStyle(
                        color: Colors.black87, fontWeight: FontWeight.bold),
                  ),
                  TextSpan(
                    text: wordcount.toString(),
                    style: TextStyle(color: Colors.black87),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
