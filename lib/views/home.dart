import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mini_proyecto/services/data-service.dart';
import 'package:mini_proyecto/views/results.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool showLoading = false;

  final searchController = TextEditingController();

  late Map<dynamic, dynamic> data;

  final Connectivity connectivity =
      Connectivity(); //Se llama el servicio connectivity para verificar la conexion a internet
  late StreamSubscription<ConnectivityResult> connectivitySubscription;
  String connectionStatus = 'Unknown'; //Status de la conexion a internet

  @override
  void initState() {
    initConnectivity(); //Iniciamos el servicio de conexion
    connectivitySubscription =
        connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  Future<void> initConnectivity() async {
    ConnectivityResult result = ConnectivityResult.none;
    try {
      result = await connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    if (mounted)
      switch (result) {
        case ConnectivityResult.wifi:
          setState(() => connectionStatus = 'En conexion');
          break;
        case ConnectivityResult.mobile:
          setState(() => connectionStatus = 'En conexion');
          break;
        case ConnectivityResult.none:
          setState(() => connectionStatus = 'Error de conexion');
          break;
        default:
          setState(() => connectionStatus = 'Error de conexion');
          break;
      }
  }

  SnackBar mySnackBar(String value) {
    return SnackBar(
      content: Text(value),
      action: SnackBarAction(
        label: 'Ok',
        onPressed: () {},
      ),
    );
  }

  void sendSearch(String word) async {
    setState(() {
      this.showLoading = true;
    });
    if (connectionStatus != "Error de conexion") {
      if (word.isEmpty) {
        setState(() {
          this.showLoading = false;
        });
        ScaffoldMessenger.of(context).showSnackBar(
            mySnackBar('Ingresa al menos un carácter en la barra de búsqueda'));
      } else {
        data = await dataService.getData(word);
        if (!data['error']) {
          setState(() {
            this.showLoading = false;
          });
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Results(
                    appBarTittle: word, data: data['data']['query']['search'])),
          );
        } else {
          setState(() {
            this.showLoading = false;
          });
          ScaffoldMessenger.of(context).showSnackBar(mySnackBar(
              'Error al realizar la busqueda. Vuelve a intentarlo más tarde'));
        }
      }
    } else {
      setState(() {
        this.showLoading = false;
      });
      ScaffoldMessenger.of(context).showSnackBar(mySnackBar(
          'Necesitas una conexión a internet para realizar la busqueda'));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ModalProgressHUD(
        //Paso 21: Agregamos un modal progress
        inAsyncCall: showLoading,
        child: Center(
          child: new ListView(
              shrinkWrap: true,
              padding: const EdgeInsets.all(20.0),
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: Container(
                      width: MediaQuery.of(context).size.width * 0.75,
                      child: Text(
                        'Buscador Wikipedia',
                        style: TextStyle(
                            fontSize: 30, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      )),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.75,
                  height: 270,
                  child: Image.asset('assets/img/main.png'),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 30),
                  child: TextField(
                    textInputAction: TextInputAction.search,
                    controller: searchController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.search),
                      hintText: 'Buscar en Wikipedia',
                      hintStyle: TextStyle(fontSize: 16),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                        borderSide: BorderSide(
                          width: 0,
                          style: BorderStyle.none,
                        ),
                      ),
                      filled: true,
                      contentPadding: EdgeInsets.all(16),
                      //fillColor: colorSearchBg,
                    ),
                    onSubmitted: (value) {
                      sendSearch(value);
                    },
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}
