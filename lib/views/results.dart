import 'package:flutter/material.dart';
import 'package:mini_proyecto/views/result-info.dart';

class Results extends StatelessWidget {
  final String appBarTittle;
  final List<dynamic> data;

  const Results({Key? key, required this.appBarTittle, required this.data})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          appBarTittle,
          style: TextStyle(color: Colors.black87),
        ),
        centerTitle: false,
        iconTheme: IconThemeData(
          color: Colors.black87, //change your color here
        ),
        automaticallyImplyLeading: true,
        backgroundColor: Colors.white,
        elevation: 1,
      ),
      body: ListView.builder(
          shrinkWrap: true,
          itemCount: data.length,
          itemBuilder: (BuildContext context, int index) => Column(
                children: [
                  ListTile(
                    title: Text(data[index]['title']),
                    trailing: GestureDetector(
                      child: Icon(Icons.arrow_forward_ios_outlined),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ResultInfo(
                                    htmlData: data[index]['snippet'],
                                    appBarTittle: data[index]['title'],
                                    pageid: data[index]['pageid'],
                                    wordcount: data[index]['wordcount'],
                                  )),
                        );
                      },
                    ),
                  ),
                ],
              )),
    );
  }
}
