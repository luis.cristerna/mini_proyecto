# mini_proyecto

Mini proyecto donde el objetivo es que por medio de la API de wikipedia se permita:
1) Consultar una búsqueda, 
2) Listar los posibles resultados y el detalle de cada uno de ellos.

# App en funcionamiento

![assets/gif/example.gif](assets/gif/example.gif "Ejemplo App")


# Icono de la App

![assets/icon/app-icon.png](assets/icon/app-icon.png "Icono")

Comandos para cambiar el icono

```
flutter pub get
flutter pub run flutter_launcher_icons:main
```

La ruta para cambiar el icono

```
assets/icon/app-icon.png
```

# Paquetes Utilizados

- [Cambiar Iconos](https://pub.dev/packages/flutter_launcher_icons)
- [Consultar APIs](https://pub.dev/packages/dio)
- [Cambio de HTML a Widget](https://pub.dev/packages/flutter_html)
- [Verificar Internet](https://pub.dev/packages/connectivity_plus)
- [Modal de progreso](https://pub.dev/packages/modal_progress_hud_nsn)

# Información acerca de las APIs de Wikipedia

- [Tutorial](https://www.mediawiki.org/wiki/API:Tutorial/es)
- [Ejemplo](https://www.mediawiki.org/wiki/Special:ApiSandbox?tableofcontents=1)

